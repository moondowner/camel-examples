package com.thisismartin.example.camel;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;

/**
 * @author Martin Spasovski (@moondowner)
 */
public class CSVExampleTest extends CamelTestSupport {

    @Test
    public void testRoute() {
        this.template.sendBody("direct:csvExample", null);
    }

    @Override
    protected RouteBuilder createRouteBuilder() {
        return new CSVExampleRoute();
    }

}
