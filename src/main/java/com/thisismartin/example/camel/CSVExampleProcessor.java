package com.thisismartin.example.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Martin Spasovski (@moondowner)
 */
public class CSVExampleProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("foo", "abc");
        body.put("bar", 123);

        exchange.getIn().setBody(body);
    }
}
