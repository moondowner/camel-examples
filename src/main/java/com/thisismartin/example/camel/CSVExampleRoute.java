package com.thisismartin.example.camel;

import org.apache.camel.builder.RouteBuilder;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Martin Spasovski (@moondowner)
 */
public class CSVExampleRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("direct:csvExample")
                .routeId("csvExample")
                .process(new CSVExampleProcessor())
                .marshal().csv().to("file:target/test_csv");
    }

}
